#!/usr/bin/python

import sys
import dbus
import dbus.service
import dbus.mainloop.glib
import gobject
import time
import threading
import vobject
import errno
import traceback
import os
import re
from optparse import OptionParser

try:
    # so we can run this outside of kodi    
    import xbmc
    import xbmcgui
    import xbmcaddon
    no_kodi=False
except ImportError:
    no_kodi=True

try:
    __addon__       = xbmcaddon.Addon(id='script.service.handsfree')
    __addondir__    = xbmc.translatePath( __addon__.getAddonInfo('profile') )
except:
    __addondir__ = "/home/pi/.kodi/userdata/addon_data/script.service.handsfree/"    
    
variables={}

BUS_NAME='org.bluez.obex'
PATH = '/org/bluez/obex'
CLIENT_INTERFACE = 'org.bluez.obex.Client1'
SESSION_INTERFACE = 'org.bluez.obex.Session1'
PHONEBOOK_ACCESS_INTERFACE = 'org.bluez.obex.PhonebookAccess1'
TRANSFER_INTERFACE = 'org.bluez.obex.Transfer1'

# Logging facility.  For some reason this randomly throws errors in the CallRemoved signal
def log(logline):
    try:
        print "HFINT: " + logline
    except:
        pass

def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)

    if no_kodi==True:
        variables[property]=value
    else:
        xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    if no_kodi==True:
        if property in variables:
            value=variables[property]
        else:
            value=""
    else:
        value=xbmcgui.Window(10000).getProperty(property)
    log("Loading: '" + property + "', '" + value + "'")
    return value

class PBAP_download():
    bus=None
    transfers=0
    device_mac=None

    def __init__(self):
        
        try:
            os.makedirs(__addondir__)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise  # raises the error again

        set_kodi_prop("PBAP_download", "1")
        paths=["PB", "CCH"]
        self.bus = dbus.SystemBus()
        
        client = dbus.Interface(self.bus.get_object(BUS_NAME, PATH), CLIENT_INTERFACE)

        # pull the info from the connected device
        self.device_mac=get_kodi_prop("connected_mac")
        log("Creating session for " + self.device_mac + " (" + get_kodi_prop("connected_device") + ")")
        try:
           session_path = client.CreateSession(self.device_mac, { "Target": "PBAP" })
        except Exception as e:
            log("PBAP: Exception creating session.")
            log(str(e))
            # see if we can read the existing files, from the last pull
            self.read_vcards(paths)
            return

        log("Session created.")        
        obj = self.bus.get_object(BUS_NAME, session_path)
        self.session = dbus.Interface(obj, SESSION_INTERFACE)
        self.pbap = dbus.Interface(obj, PHONEBOOK_ACCESS_INTERFACE)
            
        for path in paths:
        
            log("--- Select Phonebook %s ---\n" % (path))
            # If there is an error, get out of here
            try:
                self.pbap.Select("int", path)
            except Exception as e:
                log("PBAP: Exception in select.")
                log(str(e))
                set_kodi_prop("PBAP_Download", "")
                break                

            params = dbus.Dictionary({ "Format" : "vcard30",
                "Fields" : ["PHOTO"] })

            pb_mon=self.pbap.PullAll(__addondir__ + path + ".vcf", params,
            	reply_handler=self.create_transfer_reply,
                error_handler=self.error)
            # This is set here, and in the handler.
            # Mostly so the loop below will engage.
            
            set_kodi_prop("active_tx", "1")
            # track the transfers
            #tx_obj=self.bus.get_object(BUS_NAME, pb_mon[0])           
            bt_status=False
            set_kodi_prop("btconnected", "bluetooth.png")
            while get_kodi_prop("active_tx")=="1":
                if bt_status==False:
                    set_kodi_prop("btconnected", "bluetooth.png")
                else:
                    set_kodi_prop("btconnected", "bluetooth_ct.png")
                time.sleep(.25)
                bt_status = not bt_status

		set_kodi_prop("btconnected", "bluetooth_ct.png")
    	log("PBAP download complete.")
        set_kodi_prop("PBAP_download", "")            
        self.read_vcards(paths)
        
        
    def create_transfer_reply(self, path, properties):
        self.msg_bdy_props = properties
        log("Transfer created: %s (file %s)" % (path, self.msg_bdy_props["Filename"]))
        set_kodi_prop("active_tx", "1")       
        
    def error(self, err):
        log(err)

    def read_vcards(self, paths):
        pb_entries=0
        ich_entries=0
        och_entries=0
        mch_entries=0        
        # now read them in
        for path in paths:
            log ("Reading: " + path)
            try:
                file=open(__addondir__ + path + ".vcf", "r")
            except Exception as e:
                log("PBAP: Exception opening vcard info.")
                log(str(e))                    
                continue
            
            vcf=file.read()
            file.close()
            vcards=vobject.readComponents(vcf)
            for vcard in vcards:
                if path=="PB":
                    tel_list=[]
                    tel_type_list=[]
                    photo=None        
                    for data in vcard.getChildren():
                        if data.name == "FN":
                            fn=data.value
                        if data.name == "PHOTO":
                            photo=data.value
                        
                        if data.params:
                            if data.name == "TEL":
                                if len(data.value)>5:
                                    tel_list.append(data.value)
                                    for key in data.params.keys():
                                        for pbtype in data.params[key]:
                                            tel_type_list.append(pbtype)
                                            
                    if fn!="" and len(tel_list)>0:
                        pb_entries+=1
 #                       log (fn + ": ")
                        i=len(tel_list)
                        j=0
                        while j<i:
#                            log("tel: " + str(tel_type_list[j]) + ": " +  str(tel_list[j]))
                            j+=1
                        if photo!=None:
                            pfile=open(__addondir__ + fn + ".jpg", "wb")
                            pfile.write(photo)
                            pfile.close()
                            
                        

                if path=="CCH":
                    for data in vcard.getChildren():
                        if data.name=="FN":
                            fn=data.value
                        if data.name=="TEL":
                            number=data.value
                    
                        if data.name=="X-IRMC-CALL-DATETIME":
                            datetime=data.value
                            for key in data.params.keys():
                                call_type=data.params[key][0]

                    if fn=="":
                        fn=number
#                    log( call_type + " -- (" + fn + "): " + number + " @ " + datetime)
                    if call_type=="DIALED":
                        och_entries+=1
                    else:
                        if call_type=="RECEIVED":
                            ich_entries+=1
                        else:
                            if call_type=="MISSED":
                                mch_entries+=1
                                    
class HandsFree():
  
    bus=None

    voice_man=None
    call_vol=None
    netman=None
    hf=None
    manager=None
    hf_am=None
    connected_mac=None
    active_calls=None
    
    def __init__(self):

       
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)        
        self.bus = dbus.SystemBus()
  
        # ofono isn't running
        while self.manager==None:
            try:
                self.manager = dbus.Interface(self.bus.get_object('org.ofono',
                '/'), dbus_interface = 'org.ofono.Manager')
                log("Ofono is registered with dbus.")
            except:
                time.sleep(5)

        self.hf_am = dbus.Interface(self.bus.get_object('org.ofono',
                               '/'), dbus_interface = 'org.ofono.HandsfreeAudioManager')
        
        # without a card interface, there is nothing to do
        self.bus.add_signal_receiver(self.CardAdded,
                bus_name="org.ofono",
                dbus_interface="org.ofono.HandsfreeAudioManager",
                signal_name="CardAdded")

        self.bus.add_signal_receiver(self.CardRemoved,
                bus_name="org.ofono",
                dbus_interface="org.ofono.HandsfreeAudioManager",
                signal_name="CardRemoved")
                
        self.init_interface()

    def start(self):
        
        set_kodi_prop("active_calls", "0")
        self.mainloop = gobject.MainLoop()
        self.mainloop.run()

    def properties_changed(self, interface, properties, invalidated, path):
        # Regex might be overkill, but I want to make sure we only get client transfer messages
        result=re.match(r'.*session\d+/transfer\d+', path)
        if result:        
            if "Status" in properties:
#                print path        
#                print properties
        
                if properties['Status'] == 'complete':
                    self.transfer_complete(path)
                    return

                if properties['Status'] == 'error':
                    print properties
                    self.transfer_error(path)
                    return

    def transfer_complete(self, path):
        log("Transfer complete.")
        set_kodi_prop("active_tx", "0")
        
    def transfer_error(self, path):
        log("Transfer error.")
        set_kodi_prop("active_tx", "0")        

        
        
    def init_interface(self):
        modems = self.manager.GetModems()
        for path, properties in modems:
            online=properties["Online"]
            log("Device online: " + str(online))
            if online==1:
                set_kodi_prop("connected_mac", properties["Serial"])
            
            if "org.ofono.NetworkRegistration" in properties["Interfaces"]:
                self.netman=dbus.Interface(self.bus.get_object('org.ofono', path),  
                'org.ofono.NetworkRegistration')
                
            if "org.ofono.VoiceCallManager" in properties["Interfaces"]:
                self.voice_man=dbus.Interface(self.bus.get_object('org.ofono', path),  
                                                        'org.ofono.VoiceCallManager')

            if "org.ofono.CallVolume" in properties["Interfaces"]:
                self.call_vol=dbus.Interface(self.bus.get_object('org.ofono', path),
                                                        'org.ofono.CallVolume')

            if "org.ofono.Handsfree" in properties["Interfaces"]:
                self.hf=dbus.Interface(self.bus.get_object('org.ofono', path),
                                                        'org.ofono.Handsfree')
            
        cards=self.hf_am.GetCards()
            
        # card path
        if len(cards)>0:
            log("Current card: " + cards[0][0])
                
        self.bus.add_signal_receiver(self.card_prop_change,
            bus_name="org.ofono",
            dbus_interface="org.ofono.HandsfreeAudioCard",
            signal_name="PropertyChanged")

        self.bus.add_signal_receiver(self.CallAdded,
        	bus_name="org.ofono",
            dbus_interface="org.ofono.VoiceCallManager",
            signal_name="CallAdded")

        self.bus.add_signal_receiver(self.CallRemoved,
        	bus_name="org.ofono",
            dbus_interface="org.ofono.VoiceCallManager",
            signal_name="CallRemoved")

        self.bus.add_signal_receiver(self.VC_prop_change,
        	bus_name="org.ofono",
            dbus_interface="org.ofono.VoiceCallManager",
            signal_name="PropertyChanged")

        self.bus.add_signal_receiver(self.properties_changed,
        	dbus_interface="org.freedesktop.DBus.Properties",
            signal_name="PropertiesChanged", path_keyword="path")

            
        if get_kodi_prop("connected_mac")!="":
            threading.Thread(target=PBAP_download).start()
            
    def CardAdded(self, path, properties):
#       print properties
        log("Card Added.")
        # If the card was added, we just want to wait until the bluetooth poller picks it up
        self.init_interface()

        
    def CardRemoved(self, path):
        # reset kodi properties
        log("Card removed: " + path)
        
    def CallAdded(self, path, properties):
        log("Adding call: " + path)
        set_kodi_prop("hfp_close", "")
        
        self.active_calls=int(get_kodi_prop("active_calls"))
        self.active_calls+=1
        set_kodi_prop("active_calls", str(self.active_calls))
        log("Active calls: " + str(self.active_calls))
        if get_kodi_prop("handsfree_active") != "1":
            log ("Call added (" + properties["State"] + "): " + properties["LineIdentification"])
            set_kodi_prop("call_id", properties["LineIdentification"])
            set_kodi_prop("call_state", properties["State"])
            set_kodi_prop("call_direction", properties["State"])            
            set_kodi_prop("call_path", path)
            set_kodi_prop("handsfree_active", "1")
            if no_kodi==False:
                xbmc.executebuiltin('XBMC.RunScript("script.program.handsfree")')
        else:
            log("Secondary call. Not launching GUI")
            
    def CallRemoved(self, path):
        log("Removing call: " + path)
        self.active_calls=int(get_kodi_prop("active_calls"))
        self.active_calls-=1
        if self.active_calls<0:
            self.active_calls=0
        
        set_kodi_prop("active_calls", str(self.active_calls))
        if self.active_calls==0:
            set_kodi_prop("hfp_close", "1")
        log("Call removed NOW")

    def VC_prop_change(self, name, value):
        log("VC property change: '" + name + "': '" + value + "'")
        
    def card_prop_change(self, name, value):
        log("Card property change: '" + name + "': '" + value + "'")
        
    def end(self):
        if (self.mainloop):
            log("ENDING.")
            self.mainloop.quit();
       
def kodi_mon():
### standalone    
    monitor = xbmc.Monitor()
    while True:
### standalone        
        if monitor.waitForAbort(1):
            HandsFree.end(hf)
            break

if  __name__ == '__main__':
        
    gobject.threads_init()
    log ("Starting up")

    if no_kodi==True:
        set_kodi_prop("connected_mac", "E4:FA:ED:44:F8:C9")
        
    if no_kodi==False:    
        threading.Thread(target=kodi_mon).start()
    hf=HandsFree()
    hf.start()
