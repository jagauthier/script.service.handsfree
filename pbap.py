#!/usr/bin/python

import sys
import dbus
import dbus.service
import dbus.mainloop.glib
import gobject
import time
import threading
import vobject

BUS_NAME='org.bluez.obex'
PATH = '/org/bluez/obex'
CLIENT_INTERFACE = 'org.bluez.obex.Client1'
SESSION_INTERFACE = 'org.bluez.obex.Session1'
PHONEBOOK_ACCESS_INTERFACE = 'org.bluez.obex.PhonebookAccess1'
TRANSFER_INTERFACE = 'org.bluez.obex.Transfer1'

def log(logline):
    print "HFINT: " + logline

def set_kodi_prop(property, value):
    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
#    xbmcgui.Window(10000).setProperty(property, strvalue)

#def get_kodi_prop(property):
#    value=xbmcgui.Window(10000).getProperty(property)
#    log("Loading: '" + property + "', '" + value + "'")
#    return value

class PBAP_download():
    bus=None
    transfers=0
    
    def __init__(self):
        return
        paths=["PB", "CCH"]
        
        self.bus = dbus.SessionBus()
        
        client = dbus.Interface(self.bus.get_object(BUS_NAME, PATH), CLIENT_INTERFACE)

        # pull the info from the connected device in from the BT Poller
        log("Creating session.")
        session_path = client.CreateSession("E4:FA:ED:44:F8:C9", { "Target": "PBAP" })
        log("Session created.")        
        obj = self.bus.get_object(BUS_NAME, session_path)
        self.session = dbus.Interface(obj, SESSION_INTERFACE)
        self.pbap = dbus.Interface(obj, PHONEBOOK_ACCESS_INTERFACE)
            
        for path in paths:
        
            print("--- Select Phonebook %s ---\n" % (path))
            self.pbap.Select("int", path)
            params = dbus.Dictionary({ "Format" : "vcard30",
                "Fields" : ["PHOTO"] })

            pb_mon=self.pbap.PullAll("/tmp/" + path + ".vcf", params)
             
            # track the transfers
            tx_obj=self.bus.get_object(BUS_NAME, pb_mon[0])           
            
            # Bad code
            # I am going to assume success- sort of.
            # I am catching and ignoring exceptions
            # If it did fail, then we'll see what happens later
            while 1:
                try:
                    tx_st=tx_obj.Get(TRANSFER_INTERFACE, "Status", dbus_interface="org.freedesktop.DBus.Properties")
                except:
                    break
                time.sleep(.1)
            log("PBAP download complete.")
        read_vcards(paths)


def read_vcards(paths):
        pb_entries=0
        ich_entries=0
        och_entries=0
        mch_entries=0        
        # now read them in
        for path in paths:
            print "Reading: " + path
            file=open("/tmp/" + path + ".vcf")
            vcf=file.read()
            file.close()
            vcards=vobject.readComponents(vcf)
            for vcard in vcards:

                if path=="PB":
                    tel_list=[]
                    tel_type_list=[]
                    photo=None
                    for data in vcard.getChildren():
                        if data.name == "FN":
                            fn=data.value
                        if data.name == "PHOTO":
                            photo=data.value
                            print "DATA:" 
                            print data
                            for key in data.params.keys():
                                print key
                                if key=="TYPE":
                                    for phinfo in data.params[key]:
                                        if phinfo!="JPEG":
                                            print "Found something none jpeg"
                                if key=="DATA":
                                    for phinfo in data.params[key]:
                                        if phinfo!="JPEG":
                                            print "Found something none jpeg"
                                    
                                
                        
                        if data.params:
                            if data.name == "TEL":
                                if len(data.value)>5:
                                    tel_list.append(data.value)
                                    for key in data.params.keys():
                                        for pbtype in data.params[key]:
                                            tel_type_list.append(pbtype)
                                            
                    if fn!="" and len(tel_list)>0:
                        pb_entries+=1
                        if photo:
                            pfile=open("/tmp/" + fn + ".jpg", "wb")
                            pfile.write(photo)
                            pfile.close()
                            
#                        print fn + ": "
                        i=len(tel_list)
                        j=0
                        while j<i:
#                            print "tel: " + str(tel_type_list[j]) + ": " +  str(tel_list[j])
                            j+=1



                if path=="CCH":
                    for data in vcard.getChildren():
                        if data.name=="FN":
                            fn=data.value
                        if data.name=="TEL":
                            number=data.value
                    
                        if data.name=="X-IRMC-CALL-DATETIME":
                            datetime=data.value
                            for key in data.params.keys():
                                call_type=data.params[key][0]

                    if fn=="":
                        fn=number
                    print call_type + " -- (" + fn + "): " + number + " @ " + datetime
                    if call_type=="DIALED":
                        och_entries+=1
                    else:
                        if call_type=="RECEIVED":
                            ich_entries+=1
                        else:
                            if call_type=="MISSED":
                                mch_entries+=1
                                    
class HandsFree():
    bus=None

    voice_man=None
    call_vol=None
    netman=None
    hf=None
    manager=None
    hf_am=None
    
    def __init__(self):
        
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)        
        self.bus = dbus.SystemBus()
        
        # ofono isn't running
        while self.manager==None:
            try:
                self.manager = dbus.Interface(self.bus.get_object('org.ofono',
                '/'), dbus_interface = 'org.ofono.Manager')
                log("Ofono is registered with dbus.")
            except:
                time.sleep(5)

        self.hf_am = dbus.Interface(self.bus.get_object('org.ofono',
                               '/'), dbus_interface = 'org.ofono.HandsfreeAudioManager')
        
        # without a card interface, there is nothing to do
        self.bus.add_signal_receiver(self.CardAdded,
                bus_name="org.ofono",
                dbus_interface="org.ofono.HandsfreeAudioManager",
                signal_name="CardAdded")

        self.bus.add_signal_receiver(self.CardRemoved,
                bus_name="org.ofono",
                dbus_interface="org.ofono.HandsfreeAudioManager",
                signal_name="CardRemoved")
                
        self.init_interface()

    def start(self):
        
    # If we're not connected to a BT device, we really just want to stay here.
        # So, we'll just wait until we are connected, then proceed.
        
        # Start by downloading all the stuffs
        
#        threading.Thread(target=PBAP_download).start()
        self.mainloop = gobject.MainLoop()
        self.mainloop.run()
    
    def init_interface(self):
        modems = self.manager.GetModems()

        for path, properties in modems:
            print "path: " + path
            online=properties["Online"]
            print online
            if online==1:
                print properties["Serial"]

            if "org.ofono.NetworkRegistration" in properties["Interfaces"]:
                self.netman=dbus.Interface(self.bus.get_object('org.ofono', path),  
                'org.ofono.NetworkRegistration')
                
            if "org.ofono.VoiceCallManager" in properties["Interfaces"]:
                self.voice_man=dbus.Interface(self.bus.get_object('org.ofono', path),  
                                                        'org.ofono.VoiceCallManager')

            if "org.ofono.CallVolume" in properties["Interfaces"]:
                self.call_vol=dbus.Interface(self.bus.get_object('org.ofono', path),
                                                        'org.ofono.CallVolume')

            if "org.ofono.Handsfree" in properties["Interfaces"]:
                self.hf=dbus.Interface(self.bus.get_object('org.ofono', path),
                                                        'org.ofono.Handsfree')
            
            cards=self.hf_am.GetCards()
            
            # card path
            if len(cards)>0:
                log("Current card: " + cards[0][0])
            self.bus.add_signal_receiver(self.card_prop_change,
                bus_name="org.ofono",
                dbus_interface="org.ofono.HandsfreeAudioCard",
                signal_name="PropertyChanged")

            self.bus.add_signal_receiver(self.CallAdded,
                bus_name="org.ofono",
                dbus_interface="org.ofono.VoiceCallManager",
                signal_name="CallAdded")

            self.bus.add_signal_receiver(self.CallRemoved,
                bus_name="org.ofono",
                dbus_interface="org.ofono.VoiceCallManager",
                signal_name="CallRemoved")

            self.bus.add_signal_receiver(self.VC_prop_change,
                bus_name="org.ofono",
                dbus_interface="org.ofono.VoiceCallManager",
                signal_name="PropertyChanged")
            
    def CardAdded(self, path, properties):
        log("Card Added.")
        # These are paired devices with bluez.
        print self
        
        self.init_interface()
        
    def CardRemoved(self, path):
        # reset kodi properties
        log("Card removed: " + path)
        
    def CallAdded(self, path, properties):
        log ("Call added (" + properties["State"] + "): " + properties["LineIdentification"])
        cm = dbus.Interface(self.bus.get_object('org.ofono',
                path), dbus_interface = 'org.ofono.VoiceCall')
        print "Voice Call info:" 
        print cm
        time.sleep(3)
        print
        print
        cm.Hangup()
        

    def CallRemoved(self, path):
        log ("Call ended: " + path)

    def VC_prop_change(self, name, value):
        log("Call property change: '" + name + "': '" + value + "'")
        
    def card_prop_change(self, name, value):
        log("Card property change: '" + name + "': '" + value + "'")
        
    def end(self):
        pass
    
            
def kodi_mon():
    
    monitor = xbmc.Monitor()
    while True:
        if monitor.waitForAbort(1):
            HandsFree.end(player)
            break


if  __name__ == '__main__':

    gobject.threads_init()
    log ("Starting up")
    hf=HandsFree()
    hf.start()

