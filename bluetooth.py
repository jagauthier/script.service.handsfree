#!/usr/bin/env python

# Dependencies:
# sudo apt-get install -y python-gobject

import threading
import time
import signal
import dbus
import dbus.service
import dbus.mainloop.glib
import gobject

import xbmc
import xbmcgui
import xbmcaddon
import os
import random

SERVICE_NAME = "org.bluez"
AGENT_IFACE = SERVICE_NAME + '.Agent1'
ADAPTER_IFACE = SERVICE_NAME + ".Adapter1"
DEVICE_IFACE = SERVICE_NAME + ".Device1"
PLAYER_IFACE = SERVICE_NAME + '.MediaPlayer1'
TRANSPORT_IFACE = SERVICE_NAME + '.MediaTransport1'

# to get album art
addon       = xbmcaddon.Addon(id='script.service.albumart')
addondir    = xbmc.translatePath(addon.getAddonInfo('profile') )

def log(logline):
    print "BTPOLL: " + logline

def set_kodi_prop(property, value):
    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    value=xbmcgui.Window(10000).getProperty(property)
    log("Loading: '" + property + "', '" + value + "'")
    return value

class BlueAgent(dbus.service.Object):
    AGENT_PATH = "/blueagent5/agent"
    CAPABILITY = "DisplayOnly"
    pin_code = None

    def __init__(self, pin_code):
        dbus.service.Object.__init__(self, dbus.SystemBus(), BlueAgent.AGENT_PATH)
        self.pin_code = pin_code

        log("Starting BlueAgent with PIN [{}]".format(self.pin_code))
        
       
        log("BlueAgent is waiting to pair with device")

        
class BlueInfo(dbus.service.Object):
    bus = None
    mainloop = None
    device = None
    deviceAlias = None
    player = None
    connected = None
    state = None
    status = None
    track = []

    def __init__(self):
        
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        dbus.service.Object.__init__(self, dbus.SystemBus(), BlueAgent.AGENT_PATH)
        
        self.bus = dbus.SystemBus()

        self.bus.add_signal_receiver(self.changeHandler,
                bus_name="org.bluez",
                dbus_interface="org.freedesktop.DBus.Properties",
                signal_name="PropertiesChanged",
                path_keyword="path")

        self.getBTInfo()

        self.findPlayer()
        self.update_kodi_props()
        self.pin_code=str(random.randint(1000,9999))

        set_kodi_prop("pairing_code", str(self.pin_code))
        self.registerAsDefault()
   
    def start(self):
        self.mainloop = gobject.MainLoop()
        self.mainloop.run()
        
    def end(self):
        if (self.mainloop):
            log("ENDING.")
            self.mainloop.quit();
        
    @dbus.service.method(AGENT_IFACE, in_signature="os", out_signature="")
    def DisplayPinCode(self, device, pincode):
        log("BlueAgent DisplayPinCode invoked")

    @dbus.service.method(AGENT_IFACE, in_signature="ouq", out_signature="")
    def DisplayPasskey(self, device, passkey, entered):
        log("BlueAgent DisplayPasskey invoked")

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="s")
    def RequestPinCode(self, device):
        log("BlueAgent is pairing with device [{}]".format(device))
        self.trustDevice(device)
        return self.pin_code

    @dbus.service.method(AGENT_IFACE, in_signature="ou", out_signature="")
    def RequestConfirmation(self, device, passkey):
        """Always confirm"""
        log("BlueAgent is pairing with device [{}]".format(device))
        self.trustDevice(device)
        return

    @dbus.service.method(AGENT_IFACE, in_signature="os", out_signature="")
    def AuthorizeService(self, device, uuid):
        """Always authorize"""
        log("BlueAgent AuthorizeService method invoked")
        return

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="u")
    def RequestPasskey(self, device):
        log("RequestPasskey returns 0")
        return dbus.UInt32(0)

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="")
    def RequestAuthorization(self, device):
        """Always authorize"""
        log("BlueAgent is authorizing device [{}]".format(self.device))
        return

    @dbus.service.method(AGENT_IFACE, in_signature="", out_signature="")
    def Cancel(self):
        log("BlueAgent pairing request canceled from device [{}]".format(self.device))

    def trustDevice(self, path):
        bus = dbus.SystemBus()
        device_properties = dbus.Interface(bus.get_object(SERVICE_NAME, path), "org.freedesktop.DBus.Properties")
        device_properties.Set(DEVICE_IFACE, "Trusted", True)

    def registerAsDefault(self):
        defmanager = dbus.Interface(self.bus.get_object(SERVICE_NAME, "/org/bluez"), "org.bluez.AgentManager1")
        defmanager.RegisterAgent(BlueAgent.AGENT_PATH, BlueAgent.CAPABILITY)
        defmanager.RequestDefaultAgent(BlueAgent.AGENT_PATH)

#    def startPairing(self):
#        adapter_path = findAdapter().object_path
#        adapter = dbus.Interface(self.bus.get_object(SERVICE_NAME, adapter_path), "org.freedesktop.DBus.Properties")
#        adapter.Set(ADAPTER_IFACE, "Discoverable", True)
            
    def getBTInfo(self):
        paired_devices=0
        set_kodi_prop("bt_avail", "0")
        set_kodi_prop("paired_devices", "0")
        set_kodi_prop("bt_power", "0")
        set_kodi_prop("bt_discoverable", "0")
        set_kodi_prop("bt_pairable", "0")        
        set_kodi_prop("connected_device", "")
        set_kodi_prop("connected_path", "")
        set_kodi_prop("btconnected", "bluetooth.png")
        manager = dbus.Interface(self.bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
        objects = manager.GetManagedObjects()

        adapter_path = None
        device_path = None
        
        # first get BT settings. like power, discoverablility, and pairing.
        for path, interfaces in objects.iteritems():
            if ADAPTER_IFACE in interfaces:
                set_kodi_prop("bt_avail", "1")
                adapter_path = path
                
                if adapter_path:
                    self.adapter=self.bus.get_object("org.bluez", path)
                    adapter_properties=self.adapter.GetAll(ADAPTER_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
                    if "Powered" in adapter_properties:
                        self.powered=adapter_properties["Powered"]
                    if "Discoverable" in adapter_properties:
                        self.discoverable=adapter_properties["Discoverable"]
                    if "Pairable" in adapter_properties:
                        self.pairable=adapter_properties["Pairable"]

                    set_kodi_prop("bt_power", str(self.powered))
                    log("Powered: " + format(self.powered))
                    set_kodi_prop("bt_discoverable", str(self.discoverable))
                    log("Discoverable: " + format(self.discoverable))
                    set_kodi_prop("bt_pairable", str(self.pairable))
                    log("Pairable: " + format(self.pairable))
            
            # now get paired devices
            if DEVICE_IFACE in interfaces:
                device_path = path
                if device_path:
                    self.device = self.bus.get_object("org.bluez", path)
                    device_properties=self.device.GetAll(DEVICE_IFACE, dbus_interface="org.freedesktop.DBus.Properties")

                    if "Paired" in device_properties:
                        if device_properties["Paired"]==1:
                            paired_devices+=1
                            set_kodi_prop("paired_devices", str(paired_devices))
                            set_kodi_prop("bt_device"+str(paired_devices),device_properties["Alias"])
                            set_kodi_prop("bt_mac"+str(paired_devices),device_properties["Address"])
                            set_kodi_prop("bt_devpath"+str(paired_devices),path)
                    if device_properties["Connected"]==1:
                        set_kodi_prop("btconnected", "bluetooth_ct.png")                        
                        set_kodi_prop("connected_device", device_properties["Alias"])
                        set_kodi_prop("connected_path", path)

    def findPlayer(self):
        manager = dbus.Interface(self.bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
        objects = manager.GetManagedObjects()
        
        player_path = None
        for path, interfaces in objects.iteritems():
           if PLAYER_IFACE in interfaces:
                player_path = path
                break
            
        if player_path:
            self.getPlayer(player_path)
            player_properties = self.player.GetAll(PLAYER_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
            if "Status" in player_properties:
                self.status=player_properties["Status"]
            if "Track" in player_properties:
                self.track = player_properties["Track"]
                
    def getPlayer(self, path):
        self.player = self.bus.get_object("org.bluez", path)
        device_path = self.player.Get("org.bluez.MediaPlayer1", "Device", dbus_interface="org.freedesktop.DBus.Properties")
        self.getDevice(device_path)

    def getDevice(self, path):
        self.device = self.bus.get_object("org.bluez", path)
        self.deviceAlias = self.device.Get(DEVICE_IFACE, "Alias", dbus_interface="org.freedesktop.DBus.Properties")

    def changeHandler(self, interface, changed, invalidated, path):
        iface = interface[interface.rfind(".") + 1:]

        if iface == "Adapter1":
            self.getBTInfo()

        if iface == "Device1":
            if "Connected" in changed:
                self.getBTInfo()
        elif iface == "MediaControl1":
            if "Connected" in changed:
                self.connected = changed["Connected"]
                if changed["Connected"]:
                    self.getBTInfo()
                    self.findPlayer()
                if changed["Connected"]==1: 
                    self.player.Play(dbus_interface=PLAYER_IFACE)
                    
        elif iface == "MediaPlayer1":
            if "Track" in changed:
                self.track = changed["Track"]
                self.update_kodi_props()
            if "Status" in changed:
                self.status = (changed["Status"])
                self.update_kodi_props()                
                
    def update_kodi_props(self):
        if self.player:
            log("update_kodi_props")
            set_kodi_prop("player_status", self.status)
            if "Artist" in self.track:
                if self.track["Artist"]=="":
                    set_kodi_prop("player_artist", "No data")
                else:
                    set_kodi_prop("player_artist", self.track["Artist"])
                    self.artist=self.track["Artist"]
            if "Title" in self.track:
                if self.track["Title"]=="":
                    set_kodi_prop("player_title", "No data")
                else:
                    set_kodi_prop("player_title", self.track["Title"])
                    self.title=self.track["Title"]
            if "Album" in self.track:
                if self.track["Album"]=="":
                    set_kodi_prop("player_album", "No data")
                else:
                    set_kodi_prop("player_album", self.track["Album"])                    
                    self.album=self.track["Album"]
            if "Duration" in self.track:
                if self.track["Duration"]=="":
                    set_kodi_prop("player_duration", "0")
                else:
                    set_kodi_prop("player_duration", self.track["Duration"])
            if "Genre" in self.track:
                if self.track["Genre"]=="":
                    set_kodi_prop("player_genre", "No data")
                else:
                	set_kodi_prop("player_genre", self.track["Genre"])
            #### check skin settings here for toaster pop up
            if self.status != "paused" and self.status != None and self.status!="stopped":
                if "Artist" in self.track and "Title" in self.track:
                    album_str=""
                    artist_str=""
                    notify=""
                    artist_str=self.artist.replace("/", "_")
                    album_str =self.album.replace("/", "_")
                    artist_str=artist_str.lower()
                    album_str =album_str.lower()
                    art_dl=0

                    # TODO implement skin settings for this
                    # no popups if not configured
                    music=xbmc.getInfoLabel("Skin.HasSetting(MusicPopups)")
                    
                    
                    # check for image
                    album_image=get_kodi_prop(artist_str+"/"+album_str)
                    # no image yet.
                    album_str_notify=""
                    if album_image == "":
                        log("Album art not set.")
                        # see if we can wait. this might not work.
                        while art_dl<200:
                            time.sleep(.1)
                            album_image=get_kodi_prop(artist_str+"/"+album_str)
                            if album_image != "":
                                album_str_notify=album_image
                                break
                            else:
                                art_dl+=1
                            album_str_notify="noalbum.png"
                    else:
                        album_str_notify=album_image
                        
                           
                    if get_kodi_prop("media_player_active")=="":
                        xbmcgui.Dialog().notification(
                            self.title,self.artist+" - "+self.album,album_str_notify,4000,False)
        else:
            print("Waiting for media player")


def kodi_mon():
    
    monitor = xbmc.Monitor()
#    counter=0
    while True:
        if monitor.waitForAbort(1):
#        if 0:
            BlueInfo.end(player)
            break
#        time.sleep(1)

if __name__ == "__main__":
    
    gobject.threads_init()
    player = None
    log("Starting up")
    threading.Thread(target=kodi_mon).start()

   
#    agent = BlueAgent(random.randint(1000,9999))
#    agent.registerAsDefault()
#    agent.startPairing()
    
    player = BlueInfo()
    player.start()

            
